<?php
/**
 * @file
 * Preprocess theme hooks for the multiplier element type.
 */

/**
 * Implements template_preprocess_THEME() for multiplier elements.
 */
function template_preprocess_form_element_multiplier(&$variables) {
  $element = &$variables['element'];

  // Connects the attributes array to the element attributes.
  $variables['attributes_array'] = &$element['#attributes'];
  // Stores action buttons and removes them from the element's children.
  $variables['actions'] = $element['actions'];
  unset($element['actions']);

  $children = element_children($element);

  // Generates multiplier's table.
  $header = array();
  $rows = array();
  $descriptions = array(
    'data' => array(),
    'class' => array('description'),
  );

  $variables['table'] = array(
    '#theme' => 'table',
    '#colgroups' => $element['#colgroups'],
    '#header' => &$header,
    '#rows' => &$rows,
    '#empty' => $element['#empty'],
    '#sticky' => $element['#sticky'],
    '#attributes' => array(
      'id' => $element['#id'] . '-table',
      'class' => array('form-multiplier-table'),
    ),
    '#attached' => array(
      'css' => array(
        array(
          'data' => drupal_get_path('module', 'form_element_multiplier') . '/css/element.css',
          'type' => 'file',
        ),
      ),
    ),
  );

  // Header and descriptions row.
  foreach ($element['#row_model'] as $key => $child) {
    $header[$key] = $descriptions['data'][$key] = array('data' => NULL);
    $id_prefix = $element['#id'] . '-' . drupal_clean_css_identifier($key);

    if (!empty($child['#title'])) {
      $header[$key]['data'] = $child['#title'];
      $header[$key]['id'] = $id_prefix . '-label';
    }
    if (!empty($child['#required'])) {
      $header[$key]['data'] .= ' ' . theme_form_required_marker(array('element' => $child));
    }
    if ($key === 'selected' && $element['#js_select']) {
      $header[$key]['class'][] = 'select-all';
    }

    if (isset($child['#description'])) {
      $descriptions['data'][$key] = array(
        'data' => $child['#description'],
        'id' => $id_prefix . '-description',
      );

      if (!isset($rows['desc'])) {
        $rows['desc'] = &$descriptions;
      }
    }
  }

  // Creates the empty table row if a descriptions row exists.
  if (isset($rows['desc']) && empty($children)) {
    $rows[] = array(array(
      'data' => $element['#empty'],
      'colspan' => count($header),
    ));
  }

  // Form rows.
  foreach ($children as $index) {
    $rows[$index] = array(
      'data'  => NULL,
      'class' => array('draggable'),
    );

    foreach (array_keys($element['#row_model']) as $key) {
      $child = $element[$index][$key];

      // Adds ARIA attributes for a11y.
      if (!empty($header[$key]['data'])) {
        $child['#attributes']['aria-labelledby'] = $header[$key]['id'];
      }
      if (!empty($descriptions[$key]['data'])) {
        $child['#attributes']['aria-describedby'] = $descriptions[$key]['id'];
      }

      $rows[$index]['data'][$key] = drupal_render($child);
    }
  }

  // Sets multiplier's HTML id and classes.
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-multiplier'));
  // Adds drag'n'drop feature.
  drupal_add_tabledrag($element['#id'] . '-table', 'order', 'sibling', 'weight');
}

/**
 * Implements template_preprocess_THEME() for multiplier element wrappers.
 */
function template_preprocess_form_element_multiplier_wrapper(&$variables) {
  $element = &$variables['element'];
  $variables['attributes_array']['id'] = $element['#id'] . '-wrapper';
}
