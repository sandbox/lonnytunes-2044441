- Message to inform user that he needs to save the form after changes on the multiplier (like tabledrag)
- Test with file input form element
- Handle element keys collision (with default element weight)
- Pager ?
- Collapsible ?

DONE:
- #min_rows and #max_rows multiplier property
- Prevent form validation on row deletion (like for the add button)
